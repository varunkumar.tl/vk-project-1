#place this script in the root of the dockerfile
#execute the bash with the required parameters
# Usage --
#bash dockerbuild.sh imagename Tagname versionnumber

#!/bin/bash
set -ex

IMAGE_NAME=$1
TAG=$2
jfrog_registry="varunkumartl/jfrog"

docker build -t ${jfrog_registry}/${IMAGE_NAME}:${TAG} .
docker push ${jfrog_registry}/${IMAGE_NAME}:${TAG}