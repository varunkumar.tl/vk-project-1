#!/bin/bash
set -x
echo "aws config and cluster update"
aws configure set aws_access_key_id $AWS_ACCESS_KEY_ID
aws configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY
aws configure set region ap-southeast-2
aws eks --region ap-southeast-2 update-kubeconfig --name vk-cochlear-cluster2