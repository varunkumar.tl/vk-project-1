#!/usr/bin/env bash

#AWS S3 Bucket
aws_s3_bucket_name = $1
aws_region = $2

echo "========== Creating K8 S3 bucket =========="
aws s3api create-bucket \
 --bucket ${aws_s3_bucket_name} --region ${aws_region} --create-bucket-configuration  LocationConstraint=${aws_region}

echo "===========Applying S3 bucket policy to S3 bucket =======''"
aws s3api put-bucket-policy --bucket ${aws_s3_bucket_name} \
 --policy '{
	"Version": "2012-10-17",
	"Statement": [
		{
            "Effect": "Allow",
            "Principal": "*",
            "Action": [ "s3:GetObject", "s3:ListBucket" ],
            "Resource": ["arn:aws:s3:::${aws_s3_bucket_name}","arn:aws:s3:::${aws_s3_bucket_name}*"]
		}
	]
}' \
 --region ${aws_region}