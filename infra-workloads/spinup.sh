#!/usr/bin/env bash
# Copyright (c) 2020, TL Consulting Group NSW and/or its affiliates.
# Licensed under the End-User License Agreement (EULA).
# You should have received a copy of the End-User License Agreement (EULA) along with the software in a file named LICENSE.

# Description
#  This sample script creates the infrastructure services required to run jmeter in a distributed setup.
#  It is required to run this script at least one-time before running any jmeter test workloads.
#  It can also be executed to update AWS secrets when needed.
#  The script will configure the local environment to access the EKS cluster.

#
#  The following pre-requisites must be handled prior to running this script:
#    * aws CLI must be installed
#    * kubectl CLI must be installed
#    * eksctl CLI must be installed
#    * helm CLI must be installed
#    * docker CLI must be installed
#	   * envsubst must be installed
#	   * network connectivity to the database must be configured

# These environment variables needs to be set by the build agent
#    * export ORACLE_SYS_PASSWORD=
#    * export ORACLE_SCHEMA_PASSWORD=
#    * export WEBLOGIC_ADMIN_PASSWORD=
#    * export AWS_ACCESS_KEY_ID=
#    * export AWS_SECRET_ACCESS_KEY=


script="${BASH_SOURCE[0]}"
script_dir="$( cd "$( dirname "${script}" )" && pwd )"

function usage {
  echo usage: ${script} -k keyid -s secret [-h]
  echo "  -k AWS access key ID [Required]"
  echo "  -s AWS secret access key [Required]"
  # echo "  -d Oracle password for SYSDBA user, defaults to environment variable ORACLE_SYS_PASSWORD"
  # echo "  -m Oracle password for schema owner, defaults to environment variable ORACLE_SCHEMA_PASSWORD"
  # echo "  -w Weblogic admin password, defaults to environment variable WEBLOGIC_ADMIN_PASSWORD"
  echo "  -h Help"

  echo "
  
  Description
  This sample script creates the infrastructure services required to run jmeter in a distributed setup.
  It is required to run this script at least one-time before running any jmeter test workloads.
  It can also be executed to update AWS secrets when needed.
  The script will configure the local environment to access the EKS cluster.

  The following pre-requisites must be handled prior to running this script:
    * aws CLI must be installed
    * kubectl CLI must be installed
    * eksctl CLI must be installed
    * helm CLI must be installed
    * docker CLI must be installed
    * envsubst must be installed
    * network connectivity to the database must be configured"
  exit $1
}

#
# Parse the command line options
#
while getopts "hu:k:s:" opt; do
  case $opt in
    k) aws_access_key_id="${OPTARG}"
    ;;
    s) aws_secret_access_key="${OPTARG}"
    ;;
    h) usage 0
    ;;
    *) usage 0
    ;;
  esac
done

echo "Setting up the environment.."

# AWS Settings
aws_account="777635764881"
aws_profile="snsw-nonprod"
#aws_role_profile="snsw-nonprod"
aws_eks_version="1.18"
aws_region=ap-southeast-2



#ECS Settings
eks_cluster_name=snsw-eks-cluster
#eks_cidr=10.0.0.0/16
eks_min_node=3
eks_max_node=8
#eks_node_type=m5x.large
namespace=default

echo "Workspace is: ${script_dir}"

aws configure --profile ${aws_profile} set aws_access_key_id ${aws_access_key_id}
aws configure --profile ${aws_profile} set aws_secret_access_key ${aws_secret_access_key}
aws configure --profile ${aws_profile} set default.region ${aws_region}
aws configure --profile ${aws_profile} set cli_pager ""
aws configure set aws_secret_access_key default_secret_key
#aws configure set source_profile ${aws_profile} --profile ${aws_role_profile}

#export CLUSTER_NAME=${eks_cluster_name}
#export CIDR=${eks_cidr}
#export KEY=${script_dir}/keys/${key}
#export MIN_NODE=${eks_min_node}
#export MAX_NODE=${eks_max_node}
#export NODE_TYPE=${eks_node_type}
# export AWS_DEFAULT_REGION=${aws_region}
# export AWS_DEFAULT_OUTPUT=json
# export AWS_PROFILE=${aws_role_profile}
# export AWS_ACCESS_KEY_ID=${aws_access_key_id}
# export AWS_SECRET_ACCESS_KEY=${aws_secret_access_key}
# export AWS_SESSION_TOKEN=${aws_session_token}
echo Cluster Name: ${eks_cluster_name}
echo Namespace: ${namespace}
echo Node Type: $eks_node_type}

echo "Environment setup is complete"

echo "========== Creating K8 Cluster =========="

echo "checking if EKS cluster ${eks_cluster_name} already exists.."
aws eks describe-cluster --name ${eks_cluster_name} --profile ${aws_profile} --region ${aws_region}

if [ ${PIPESTATUS[0]} == "0" ]; then
	echo "cluster ${eks_cluster_name} found. Skip cluster creation."
  else
  echo "creation started, it shall take 10-15 mins to create the cluster with fargate profile"
  eksctl create cluster --profile ${aws_profile} --name ${eks_cluster_name} --version ${aws_eks_version} --region ${aws_region} --fargate --alb-ingress-access 
fi

echo "update EKS cluster config.."
aws eks update-kubeconfig --profile ${aws_profile} --name ${eks_cluster_name} --region ${aws_region}

echo "creating cluster role binding.."
kubectl create -f defaultclusteradmin.yaml

echo "setup the sealed secret controller.."
kubectl apply -f https://github.com/bitnami-labs/sealed-secrets/releases/download/v0.15.0/controller.yaml

echo "creating AWS secrets.."
(kubectl create secret generic secret-aws-cli --dry-run -o json --from-literal=username=${aws_access_key_id} --from-literal=password=${aws_secret_access_key} | kubeseal --scope strict -) > sealed-secret.json
kubectl apply -f sealed-secret.json

echo "setup the storage volume.."
kubectl apply -f storage.yaml

echo "setup the gitlab runner.."
echo "remember to set the registration token first in both gitlab and values.yaml"
helm repo add gitlab https://charts.gitlab.io
helm upgrade --install --namespace default gitlab-runner -f ./gitlab-runner/values.yaml gitlab/gitlab-runner
