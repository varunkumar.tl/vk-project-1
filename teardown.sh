#!/bin/bash

set -e

echo "Cleanup in progress.."
kubectl delete sts jmapp-${CI_JOB_ID}
kubectl delete job jmapp-${CI_JOB_ID}-runtest
kubectl delete job jmapp-${CI_JOB_ID}-sync
kubectl delete svc jmapp-${CI_JOB_ID}
kubectl delete cm workload-${CI_JOB_ID}-cm
kubectl delete cm jmapp-${CI_JOB_ID}-cm