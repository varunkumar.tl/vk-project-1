#!/bin/bash
set -e
echo "Entering into shell"
echo "Jmeter Mode = "$1
echo "Throughput = "${THROUGHPUT}
echo "Test Duration = "${DURATION}
echo "Rampup Time = "${RAMPUP}
echo "Thread Count = "${THREADCOUNT}
echo "Slave Hosts = "${IPRANGE}
echo "Workload Path = "${TESTPLAN}
echo "Workload Data = "${DATAFILE}
echo "Output Log = "${OUTPUTLOG}
echo "Output Report = "${RESULTS}
echo "Errors File = "${ERRORSFILE}

case $1 in
    master)
        echo "jmeter running in master mode"
        jmeter -v
	jmeter -n -t ${TESTPLAN} -e -o ${RESULTS} -l ${OUTPUTLOG} -R ${IPRANGE} -J ErrorsFile=${ERRORSFILE} \
        -G Throughput=${THROUGHPUT} -G TestDurationSecs=${DURATION} \
        -G rampup=${RAMPUP} -G DataFile=${DATAFILE} -G threads=${THREADCOUNT} \
        -Dclient.rmi.localport=5000 -Jserver.rmi.ssl.disable=true \
        ;;
    server)
        echo "jmeter running in slave mode"
        $JMETER_HOME/bin/jmeter-server \
            -Dserver.rmi.localport=4000 \
            -Jserver.rmi.ssl.disable=true
        ;;
    *)
        ;;
esac
